package com.example.youtubeclone

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import androidx.lifecycle.ViewModel

class ContentViewModel: ViewModel() {

    var repo:Repo? = Repo()
    var pageToken:String = ""
    var content:MutableLiveData<Content> = MutableLiveData()

    fun getData() {
        repo?.fetchResult("")?.observeForever {
            content.value = it
            Log.w("idLenght", it?.items?.get(0)?.videoId.toString().length.toString())
            pageToken = it?.nextPageToken!!
        }
    }

    fun getDataPaging(){
        repo?.results = MutableLiveData()
        repo?.fetchResult(pageToken)?.observeForever {
            for (item in it?.items!!){
                 content.value?.items?.add(item)
            }
            content.value = content.value
            pageToken = it?.nextPageToken!!

        }
    }
    fun getDataSearch(search:String) {
        repo?.results = MutableLiveData()
        repo?.fetchSearchResult("",search)?.observeForever {
            content.value = it

            for(cont in content?.value?.items!!){
                cont.videoId = cont.videoId.toString().substring(cont.videoId.toString().length-12,cont.videoId.toString().length-1)
                Log.w("idLenght", it?.items?.get(0)?.videoId.toString())
            }
            pageToken = it?.nextPageToken!!
        }
    }

    fun getDataPagingSearch(search:String){
        repo?.results = MutableLiveData()
        repo?.fetchSearchResult(pageToken,search)?.observeForever {
            for (item in it?.items!!){
                content.value?.items?.add(item)
            }
            content.value = content.value
            pageToken = it?.nextPageToken!!

        }
    }




}