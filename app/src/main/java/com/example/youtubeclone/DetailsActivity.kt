package com.example.youtubeclone

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.pm.PackageManager
import android.os.*
import android.util.Log
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import kotlinx.android.synthetic.main.activity_details.*
import android.text.method.ScrollingMovementMethod
import com.github.kiulian.downloader.YoutubeDownloader
import com.github.kiulian.downloader.model.YoutubeVideo
import com.github.kiulian.downloader.model.formats.VideoFormat
import com.github.kiulian.downloader.model.quality.VideoQuality
import java.io.File
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import java.io.FileNotFoundException
import org.jetbrains.anko.*
import java.lang.Exception


class DetailsActivity : YouTubeBaseActivity() {
    lateinit var video: YoutubeVideo
    lateinit var youtubePlayerInit: YouTubePlayer.OnInitializedListener
    lateinit var videoFormats: List<VideoFormat>

    companion object {
        var videoID: String = ""
        var videoTitle: String = ""
        var videoDesc: String = ""
        var channelTitle: String = ""
        val VIDEO_ID = "videoID"
        val VIDEO_TITLE = "videoTitle"
        val VIDEO_DESC = "videoDesc"
        val CHANNEL_TITLE = "channelTitle"
        val YOUTUBE_API_KEY: String = "AIzaSyA25Tm7NuGpohMe0CjO8-HFYARVA-CyABE"
        var isfinish: MutableLiveData<Boolean> = MutableLiveData()


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_details)
        videoDescLbl.movementMethod = ScrollingMovementMethod.getInstance()
        videoID = intent.getStringExtra(VIDEO_ID)
        videoTitle = intent.getStringExtra(VIDEO_TITLE)
        videoDesc = intent.getStringExtra(VIDEO_DESC)
        channelTitle = intent.getStringExtra(CHANNEL_TITLE)
        videoTitleLbl.text = videoTitle
        videoDescLbl.text = videoDesc
        channelTitleLbl.text = channelTitle


        initUI()
        ytPlayer.initialize(YOUTUBE_API_KEY, youtubePlayerInit)
        downBtn.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    100
                )
            } else {


                runOnUiThread {
                    var downloadVideo = DownloadThread(baseContext)
                    toast(videoTitle + " is downloading...")
                    downloadVideo.start()
                    isfinish.value = DownloadThread.isFinish
                }




            }
        }
    }





    private fun initUI() {
        youtubePlayerInit = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
                p1?.loadVideo(videoID)

            }

            override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
                Log.w("Error", "Player Error")
            }
        }

    }

    class DownloadThread(var detailsActivity: Context) : Thread() {


        companion object {
            var isFinish: Boolean? = false

        }
        var mHandler : Handler? = null

        override fun run() {
            super.run()
            Looper.prepare()
            isFinish = false
            var video: YoutubeVideo = YoutubeDownloader.getVideo(videoID)
            var videoFormats: List<VideoFormat> = video.findVideoWithQuality(VideoQuality.hd720)
            val outputDir = File(Environment.getExternalStorageDirectory().absolutePath + "/my_videos")
            try {
                video.download(videoFormats.get(0), outputDir)

            } catch (E: FileNotFoundException) {
                // Toast.makeText(ApplicationContextProvider.getContext(), videoTitle +" is unavailable to download...",Toast.LENGTH_LONG).show()
            }

            isFinish = true
            println("Download Done.")
            try {
                Toast.makeText(detailsActivity, video.details().title() + " has finished downloading!", Toast.LENGTH_LONG).show()
            }catch (e: Exception){
                println(e.message)
            }
            Looper.loop()

        }

    }
}
