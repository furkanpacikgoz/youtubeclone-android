package com.example.youtubeclone

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.example.youtubeclone.Extensions.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val viewModel:ContentViewModel = ContentViewModel()
    var flag: Boolean = true
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var isSearching: Boolean = false
    var adapter:RVAdapter = RVAdapter(ArrayList())
    var searched:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerMain.layoutManager = LinearLayoutManager(this)
        liveDataObserver()
        pageListener()

        my_swipeRefresh_Layout.setOnRefreshListener {
            if(!isSearching) {
                viewModel.getData()
            }else{
                viewModel.getDataSearch(searched)
            }
            my_swipeRefresh_Layout.isRefreshing = false
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu,menu)

        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu?.findItem(R.id.search)
        val searchView = searchItem?.actionView as SearchView

        searchView.setSearchableInfo(manager.getSearchableInfo(componentName))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(p0: String?): Boolean {
                isSearching = true
                searchView.clearFocus()
                searchView.setQuery("",false)
                searchItem.collapseActionView()
               // Toast.makeText(this@MainActivity,"Looking for $p0",Toast.LENGTH_LONG).show()

                if (p0 != null) {
                    flag = true
                    viewModel.getDataSearch(p0)
                    searched = p0
                }
                return true
            }
        })

    return true
    }

    private fun liveDataObserver() {
        viewModel.getData()
        viewModel.content.observe(this, Observer {
            it.let {
                if (flag) {
                    adapter = RVAdapter(it?.items!!)
                    recyclerMain.adapter = adapter
                    flag = false
                } else {
                    adapter.items = it?.items!!
                }

                adapter.notifyDataSetChanged()
                isLoading = false
                progress.visibility = View.GONE
                val params = my_swipeRefresh_Layout.layoutParams as ConstraintLayout.LayoutParams
                params.bottomMargin = 0
                my_swipeRefresh_Layout.requestLayout()
            }
        })
    }

    private fun pageListener() {
        recyclerMain?.addOnScrollListener(object :
            PaginationScrollListener(recyclerMain.layoutManager as LinearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                val params = my_swipeRefresh_Layout.layoutParams as ConstraintLayout.LayoutParams
                params.bottomMargin = 120
                my_swipeRefresh_Layout.requestLayout()
                progress.visibility = View.VISIBLE
                if(!isSearching) {
                    viewModel.getDataPaging()
                }else{
                    viewModel.getDataPagingSearch(searched)
                }
            }

        })
    }


}

