package com.example.youtubeclone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Content {

    @SerializedName("nextPageToken")
    @Expose
    var nextPageToken: String? = null
    @SerializedName("items")
    @Expose
    var items: ArrayList<Item>? = null

}