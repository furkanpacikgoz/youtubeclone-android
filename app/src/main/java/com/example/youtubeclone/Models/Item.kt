package com.example.youtubeclone

import com.example.youtubeclone.Models.Snippet
import com.example.youtubeclone.Models.VideoId
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Item {

    @SerializedName("snippet")
    @Expose
    var snippet: Snippet? = null
    @SerializedName("id")
    @Expose
    var videoId: Any? = null


}