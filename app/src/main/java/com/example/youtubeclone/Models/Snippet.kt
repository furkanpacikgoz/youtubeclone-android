package com.example.youtubeclone.Models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Snippet {

    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("thumbnails")
    @Expose
    var thumbnails: Thumbnails? = null
    @SerializedName("channelTitle")
    @Expose
    var channelTitle: String? = null

}