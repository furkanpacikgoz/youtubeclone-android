package com.example.youtubeclone.Models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Thumbnails {

    @SerializedName("high")
    @Expose
    var maxres: Maxres? = null

}