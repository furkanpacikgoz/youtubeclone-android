package com.example.youtubeclone.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VideoId {
    @SerializedName("videoId")
    @Expose
    var id: String? = null
}