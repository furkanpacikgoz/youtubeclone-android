package com.example.youtubeclone

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.youtube_cell.view.*

class RVAdapter( var items : ArrayList<Item>): RecyclerView.Adapter<CustomViewHolder>(){


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.youtube_cell,p0,false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(p0: CustomViewHolder, p1: Int) {
            p0.view.titleLbl.text = items[p1].snippet!!.title
            Picasso.get().load(items[p1].snippet?.thumbnails?.maxres?.url).into(p0.view.thumbImage)
            p0.view.channelTitleLbl.text = items[p1].snippet?.channelTitle
            p0.video = items[p1]

    }





}


 class CustomViewHolder(val view: View,var video: Item? = null): RecyclerView.ViewHolder(view){
     companion object {
         val VIDEO_ID = "videoID"
         val VIDEO_TITLE = "videoTitle"
         val VIDEO_DESC = "videoDesc"
         val CHANNEL_TITLE = "channelTitle"
     }

    init{
        view.setOnClickListener{
            val intent = Intent(view.context, DetailsActivity::class.java)
            intent.putExtra(VIDEO_ID,video?.videoId.toString())
            intent.putExtra(VIDEO_TITLE,video?.snippet?.title)
            intent.putExtra(VIDEO_DESC,video?.snippet?.description)
            intent.putExtra(CHANNEL_TITLE,video?.snippet?.channelTitle)
            view.context.startActivity(intent)
        }
    }
}
