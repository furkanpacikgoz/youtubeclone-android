package com.example.youtubeclone

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.youtubeclone.Service.ApiClient
import com.example.youtubeclone.Service.RestInterface
import com.example.youtubeclone.Service.SearchInterface
import retrofit2.Call
import retrofit2.Response

class Repo{

    var results: MutableLiveData<Content> = MutableLiveData()
    fun fetchResult(pageToken:String): MutableLiveData<Content>?{

        ApiClient.getClient().create(RestInterface::class.java).getRepo(pageToken).enqueue(object :retrofit2.Callback<Content>{
            override fun onFailure(call: Call<Content>, t: Throwable) {
                Log.w("ParseError",t.message)
            }

            override fun onResponse(call: Call<Content>, response: Response<Content>){
                results.value = response.body()

            }
        })
        return results
    }
    fun fetchSearchResult(pageToken:String,search:String): MutableLiveData<Content>?{

        ApiClient.getClient().create(SearchInterface::class.java).getRepo(pageToken,search).enqueue(object :retrofit2.Callback<Content>{
            override fun onFailure(call: Call<Content>, t: Throwable) {
                Log.w("ParseError",t.message)
            }

            override fun onResponse(call: Call<Content>, response: Response<Content>){
                results.value = response.body()

            }
        })
        return results
    }
}