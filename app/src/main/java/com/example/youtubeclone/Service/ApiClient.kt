package com.example.youtubeclone.Service

import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit



class ApiClient{
    companion object {
        fun getClient(): Retrofit {
           return Retrofit.Builder()
               .baseUrl("https://www.googleapis.com/youtube/v3/")
               .addConverterFactory(GsonConverterFactory.create()).build()
        }
    }

}