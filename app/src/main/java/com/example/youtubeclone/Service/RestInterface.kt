package com.example.youtubeclone.Service

import com.example.youtubeclone.Content
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Query

interface RestInterface {

    @GET("videos?part=snippet&maxResults=20&chart=mostPopular&regionCode=TR&key=AIzaSyA25Tm7NuGpohMe0CjO8-HFYARVA-CyABE")
    fun getRepo(@Query("pageToken") pageToken:String) : Call<Content>
}