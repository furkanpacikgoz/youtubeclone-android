package com.example.youtubeclone.Service

import com.example.youtubeclone.Content
import retrofit2.http.GET
import retrofit2.Call
import retrofit2.http.Query

interface SearchInterface {

    @GET("search?part=snippet&type=video&maxResults=20&key=AIzaSyA25Tm7NuGpohMe0CjO8-HFYARVA-CyABE")
    fun getRepo(@Query("pageToken") pageToken:String,@Query("q") search:String) : Call<Content>
}